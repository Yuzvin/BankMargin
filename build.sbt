name := "BankMarginTask"
 
version := "1.0" 
      
lazy val `bankmargintask` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.2"

scalacOptions += "-Ypartial-unification"

libraryDependencies ++= ProjectDependencies.dependencies

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

      