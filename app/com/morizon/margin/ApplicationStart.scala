package com.morizon.margin

import cats.implicits._
import com.google.inject.Inject
import com.morizon.margin.services.MockingService
import com.morizon.margin.slick.tables.TablesInitiator
import com.morizon.margin.utils.EmptyEitherTF

import scala.concurrent.ExecutionContext.Implicits.global

class ApplicationStart @Inject()(tablesInitiator: TablesInitiator, mockingService: MockingService) {

  runOnStartup()

  def runOnStartup(): EmptyEitherTF = {
    tablesInitiator.createTables()
    mockingService.mockMargins()
  }

}
