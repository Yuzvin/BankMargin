package com.morizon.margin.services

import javax.inject.Inject

import com.morizon.margin.dao.MarginsDao
import com.morizon.margin.models.{Margin, MarginRange, MarginRangeTypeNames, MarginSearchCriterion}
import com.morizon.margin.utils.ConfigValueReaders._
import com.morizon.margin.utils.EitherTF
import com.morizon.margin.utils.EitherTFUtils.EitherTF
import com.typesafe.config.Config
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._

import scala.concurrent.ExecutionContext.Implicits.global

trait MarginService {

  def addMargin(margin: Margin): EitherTF[Seq[Long]]

  def findMargins(paymentMonth: Int, personalShare: Int, loanAmount: Long): EitherTF[Seq[Margin]]

}

class MarginServiceImplementation @Inject()(marginsDao: MarginsDao, config: Config) extends MarginService {

  lazy val firstYearMargin      = config.as[Margin]("first-year-margin")
  lazy val firstYearMarginRange = config.as[MarginRange]("ranges.month.0-12")

  override def findMargins(paymentMonth: Int, personalShare: Int, loanAmount: Long): EitherTF[Seq[Margin]] = {
    val searchCriteria = Seq(
      MarginSearchCriterion(MarginRangeTypeNames.PaymentMonth, paymentMonth),
      MarginSearchCriterion(MarginRangeTypeNames.PersonalShare, personalShare),
      MarginSearchCriterion(MarginRangeTypeNames.LoanAmount, loanAmount)
    )
    if (searchCriteria.exists(isFirstYearSearchCriteria)) EitherTF(Seq(firstYearMargin))
    else marginsDao.findMargins(searchCriteria)
  }

  override def addMargin(margin: Margin): EitherTF[Seq[Long]] = {
    marginsDao.insertMargin(margin)
  }

  private val isFirstYearSearchCriteria: (MarginSearchCriterion) => Boolean =
    sc => sc.rangeTypeName == MarginRangeTypeNames.PaymentMonth && sc.rangeValue <= firstYearMarginRange.max
}
