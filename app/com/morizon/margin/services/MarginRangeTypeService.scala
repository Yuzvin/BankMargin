package com.morizon.margin.services

import com.google.inject.Inject
import com.morizon.margin.dao.MarginRangeTypesDao
import com.morizon.margin.models.MarginRangeType
import com.morizon.margin.utils.EitherTF


trait MarginRangeTypeService {

  def insertMarginRangeType(rangeType: MarginRangeType): EitherTF[Long]
}

class MarginRangeTypeServiceImplementation @Inject()(marginRangeTypesDao: MarginRangeTypesDao) extends MarginRangeTypeService {

  override def insertMarginRangeType(rangeType: MarginRangeType): EitherTF[Long] = marginRangeTypesDao.insertRangeType(rangeType)
}
