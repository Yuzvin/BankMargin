package com.morizon.margin.services

import com.google.inject.{Inject, Singleton}
import com.morizon.margin.errors.{ApplicationError, InvalidLoanAmount, InvalidPaymentMonth, InvalidPersonalShare}
import com.typesafe.config.Config
import net.ceedubs.ficus.Ficus._

trait ValidationService {

  def validateSearchValues(paymentMonth: Int, personalShare: Int, loanAmount: Long): Either[ApplicationError, Unit]

}

@Singleton
class ValidationServiceImplementation @Inject()(config: Config) extends ValidationService {

  lazy val maxPaymentMonth = config.as[Int]("max.payment-month")
  lazy val maxLoanAmount   = config.as[Int]("max.loan")

  override def validateSearchValues(paymentMonth: Int, personalShare: Int, loanAmount: Long): Either[ApplicationError, Unit] = {
    for {
      _ <- validateMonth(paymentMonth)
      _ <- validatePersonalShare(personalShare)
      _ <- validateLoanAmount(loanAmount)
    } yield Unit
  }

  private def validateMonth(month: Int): Either[ApplicationError, Unit] = {
    if (month < 0 || month > maxPaymentMonth) Left(InvalidPaymentMonth(month)) else Right(Unit)
  }

  private def validatePersonalShare(share: Int) = {
    if (share < 10 || share >= 100) Left(InvalidPersonalShare(share)) else Right(Unit)
  }

  private def validateLoanAmount(amount: Long) = {
    if (amount <= 0 || amount > maxLoanAmount) Left(InvalidLoanAmount(amount)) else Right(Unit)
  }
}
