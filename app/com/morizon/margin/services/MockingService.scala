package com.morizon.margin.services
import javax.inject.Inject

import cats.implicits._
import com.google.inject.Singleton
import com.morizon.margin.models.{Margin, MarginRangeType}
import com.morizon.margin.utils.ConfigValueReaders._
import com.morizon.margin.utils.EitherTFUtils._
import com.morizon.margin.utils.EmptyEitherTF
import com.typesafe.config.Config
import net.ceedubs.ficus.Ficus._

import scala.concurrent.ExecutionContext.Implicits.global

trait MockingService {

  def mockMargins(): EmptyEitherTF

  def getMockedMargins(): Seq[Margin]

}

@Singleton
class MockingServiceImplementation @Inject()(config: Config,
                                             marginService: MarginService,
                                             marginRangeTypeService: MarginRangeTypeService)
    extends MockingService {

  lazy val mockedMargins    = config.as[Seq[Margin]]("margins")
  lazy val mockedRangeTypes = config.as[Seq[MarginRangeType]]("range-types-data")

  override def mockMargins(): EmptyEitherTF = {
    for {
      rangeTypesIds <- mockedRangeTypes.map(marginRangeTypeService.insertMarginRangeType).foldable
      marginsWithRealTypes = getMarginsWithRealMarginTypes(rangeTypesIds)
      _ <- marginsWithRealTypes.map(marginService.addMargin).foldable
    } yield ()
  }

  override def getMockedMargins() = mockedMargins

  private def getMarginsWithRealMarginTypes(rangeTypesIds: Seq[Long]) = {
    val rangeNameTypesMap = mockedRangeTypes
      .zip(rangeTypesIds)
      .map { case (rangeType, id) => rangeType.name -> rangeType.copy(id = Some(id)) }
      .toMap

    mockedMargins.map { margin =>
      margin.copy(ranges = margin.ranges.map { range =>
        range.copy(rangeType = rangeNameTypesMap(range.rangeType.name))
      })
    }
  }

}
