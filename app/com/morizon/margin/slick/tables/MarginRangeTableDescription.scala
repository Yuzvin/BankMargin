package com.morizon.margin.slick.tables

trait MarginRangeTableDescription extends MarginRangeTypesTableDescription {

  import profile.api._

  case class MarginRangeRow(id: Long, marginRangeTypeId: Long, minValue: Long, maxValue: Long)

  class MarginRangeTable(tag: Tag) extends Table[MarginRangeRow](tag, "ranges") {

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def rangeTypeId = column[Long]("range_type_id")
    def minValue = column[Long]("min_value")
    def maxValue = column[Long]("max_value")

    def rangeGroup = foreignKey("range_group_fk", rangeTypeId, MarginRangeTypes)(
      _.id, onUpdate = ForeignKeyAction.Cascade, onDelete = ForeignKeyAction.Cascade
    )

    override def * = (id, rangeTypeId, minValue, maxValue) <> (MarginRangeRow.tupled, MarginRangeRow.unapply)
  }

  lazy val MarginRanges = TableQuery[MarginRangeTable]

}
