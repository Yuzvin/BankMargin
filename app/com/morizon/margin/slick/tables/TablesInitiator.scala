package com.morizon.margin.slick.tables

import javax.inject.Inject

import com.morizon.margin.utils.EitherTFUtils._
import com.morizon.margin.utils.EmptyEitherTF
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.meta.MTable

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class TablesInitiator @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
    extends MarginRangeTypesTableDescription
    with MarginRangeTableDescription
    with MarginValueTableDescription
    with MarginToRangesTableDescription {

  import profile.api._

  def createTables(): Unit = {

    val tables = Seq(MarginValues, MarginRangeTypes, MarginRanges, MarginToRanges)

    Await.result(db.run(MTable.getTables).map { existingTables =>
      tables.foreach {
        table =>
          if (!existingTables.exists(_.name.name == table.baseTableRow.tableName))
            Await.result(db.run(table.schema.create), 10.seconds)
      }
    }, 10.second)

  }

}
