package com.morizon.margin.slick.tables

import play.api.db.slick.HasDatabaseConfigProvider

trait MarginRangeTypesTableDescription extends HasDatabaseConfigProvider[slick.jdbc.JdbcProfile]{

  import profile.api._

  case class MarginRangeTypeRow(id: Long, name: String, unit: String)

  class MarginRangeTypeTable(tableTag: Tag) extends Table[MarginRangeTypeRow](tableTag, "range_types") {

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def unit = column[String]("unit")

    override def * = (id, name, unit) <> (MarginRangeTypeRow.tupled, MarginRangeTypeRow.unapply)
  }

  lazy val MarginRangeTypes = TableQuery[MarginRangeTypeTable]
}
