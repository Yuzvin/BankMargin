package com.morizon.margin.slick.tables

trait MarginToRangesTableDescription extends MarginValueTableDescription with MarginRangeTableDescription {

  import profile.api._

  case class MarginToRangeRow(id: Long, marginId: Long, marginRangeId: Long)

  class MarginToRangesTable(tag: Tag) extends Table[MarginToRangeRow](tag, "values_to_ranges") {

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def marginId = column[Long]("margin_value_id")
    def marginRangeId = column[Long]("range_id")

    override def * = (id, marginId, marginRangeId) <> (MarginToRangeRow.tupled, MarginToRangeRow.unapply)

  }

  lazy val MarginToRanges = TableQuery[MarginToRangesTable]

}
