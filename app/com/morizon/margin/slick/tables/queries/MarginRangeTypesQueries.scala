package com.morizon.margin.slick.tables.queries

import com.morizon.margin.models.MarginRangeType
import com.morizon.margin.slick.tables.MarginRangeTypesTableDescription

trait MarginRangeTypesQueries extends MarginRangeTypesTableDescription {

  import profile.api._

  def selectMarginRangeType(id: Long) = {
    MarginRangeTypes.filter(_.id === id).result.headOption
  }

  def insertRangeType(marginRangeType: MarginRangeTypeRow) = {
    MarginRangeTypes returning MarginRangeTypes.map(_.id) += marginRangeType
  }

  def selectAllMarginRangeTypes() = MarginRangeTypes.result

  def toMarginRangeTypeRow(marginRangeType: MarginRangeType): MarginRangeTypeRow = {
    MarginRangeTypeRow(
      id = marginRangeType.id.getOrElse(0),
      name = marginRangeType.name,
      unit = marginRangeType.units
    )
  }

}
