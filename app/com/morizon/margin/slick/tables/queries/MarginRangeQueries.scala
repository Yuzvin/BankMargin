package com.morizon.margin.slick.tables.queries

import com.morizon.margin.slick.tables.{MarginRangeTableDescription, MarginRangeTypesTableDescription}

trait MarginRangeQueries extends MarginRangeTypesTableDescription with MarginRangeTableDescription with MarginQueries {

  import profile.api._

  def insertMarginRangeRow(marginRangeRow: MarginRangeRow) = {
    MarginRanges returning MarginRanges.map(_.id) += marginRangeRow
  }

  def selectAllMarginRanges() = MarginRanges.result

}
