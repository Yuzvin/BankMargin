package com.morizon.margin.slick.tables.queries

import com.morizon.margin.slick.tables.MarginToRangesTableDescription

trait MarginToRangesQueries extends MarginToRangesTableDescription {

  import profile.api._

  def insertMarginToRange(marginToRangeRow: MarginToRangeRow) = {
    MarginToRanges returning MarginToRanges.map(_.id) += marginToRangeRow
  }

}
