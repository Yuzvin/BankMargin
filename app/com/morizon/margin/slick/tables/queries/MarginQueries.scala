package com.morizon.margin.slick.tables.queries

import com.morizon.margin.models.MarginSearchCriterion
import com.morizon.margin.slick.tables.{MarginRangeTableDescription, MarginRangeTypesTableDescription, MarginToRangesTableDescription, MarginValueTableDescription}

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

trait MarginQueries
  extends MarginRangeTableDescription
    with MarginValueTableDescription
    with MarginRangeTypesTableDescription
    with MarginToRangesTableDescription {

  import profile.api._

  case class DBSearchResult(marginId: Long,
                            marginValue: Double,
                            marginTypeName: String,
                            marginTypeUnit: String,
                            marginMinValue: Long,
                            marginMaxValue: Long)

  def createSchemaUnit = {
    val tables = Seq(MarginRangeTypes, MarginRanges, MarginValues, MarginToRanges)

    val schema = tables.map(_.schema).reduce(_ ++ _)
    Await.result(db.run(schema.create), 10.seconds)
  }

  def selectMargins(criteria: Seq[MarginSearchCriterion]): DBIOAction[Seq[DBSearchResult], NoStream, Effect.Read] = {
    val queries = criteria.map { case MarginSearchCriterion(marginType, value) => selectMargin(marginType, value) }
    DBIO.sequence(queries).map(_.flatten.map(DBSearchResult.tupled))
  }

  def selectMargin(marginType: String, value: Long) = {
    MarginValues
      .join(MarginToRanges)
      .on { case (marginValue, valueRange) => marginValue.id === valueRange.marginId }
      .join(MarginRanges)
      .on { case ((_, valueRange), range) => range.id === valueRange.marginRangeId }
      .join(MarginRangeTypes)
      .on { case ((_, range), rangeType) => rangeType.id === range.rangeTypeId }
      .filter {
        case ((_, range), rangeType) =>
          range.minValue <= value && range.maxValue > value && rangeType.name === marginType
      }
      .map {
        case (((marginValue, _), range), rangeType) =>
          (marginValue.id, marginValue.value, rangeType.name, rangeType.unit, range.minValue, range.maxValue)
      }.result
  }

  def insertMarginValue(marginValue: MarginValueRow) = {
    MarginValues returning MarginValues.map(_.id) += marginValue
  }



}
