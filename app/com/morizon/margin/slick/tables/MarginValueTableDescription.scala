package com.morizon.margin.slick.tables

import play.api.db.slick.HasDatabaseConfigProvider

trait MarginValueTableDescription extends HasDatabaseConfigProvider[slick.jdbc.JdbcProfile] {

  import profile.api._

  case class MarginValueRow(id: Long, value: Double)

  class MarginValueTable(tableTag: Tag) extends Table[MarginValueRow](tableTag, "margin_values") {

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def value = column[Double]("value")

    override def * = (id, value) <> (MarginValueRow.tupled, MarginValueRow.unapply)

  }

  lazy val MarginValues = TableQuery[MarginValueTable]

}
