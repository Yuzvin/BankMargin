package com.morizon.margin.errors
import com.morizon.margin.errors.ErrorMessages._

case class InvalidPersonalShare(value: Int) extends ApplicationError(invalidPersonalShare(value))

case class InvalidLoanAmount(value: Long) extends ApplicationError(invalidLoanAmount(value))

case class InvalidPaymentMonth(value: Int) extends ApplicationError(invalidPaymentMonth(value))


