package com.morizon.margin.errors

object ErrorMessages {
  val InternalDatabaseErrorMessage = "Internal database error"
  val InternalServerErrorMessage = "Internal server error"

  def invalidPersonalShare(value: Int) = s"Invalid personal share: $value"
  def invalidLoanAmount(value: Long) = s"Invalid loan amount: $value"
  def invalidPaymentMonth(value: Int) = s"Invalid payment month: $value"

}
