package com.morizon.margin.errors
import com.morizon.margin.errors.ErrorMessages._

case object InternalDatabaseError extends ApplicationError(InternalDatabaseErrorMessage)

case object InternalServerError extends ApplicationError(InternalServerErrorMessage)