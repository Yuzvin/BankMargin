package com.morizon.margin.errors

import play.api.libs.json.Json

abstract class ApplicationError(val errorMessage: String) {
  def toJson = Json.obj("errorMessage" -> errorMessage)
}

