package com.morizon.margin.utils

import cats.data.EitherT
import cats.implicits._
import cats.{Foldable, Monoid}
import com.morizon.margin.errors.ApplicationError
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.Result
import play.api.mvc.Results._

import scala.concurrent.{ExecutionContext, Future}

object EitherTFUtils {

  object EitherTF {

    def apply[T](rightValue: T)(implicit ec: ExecutionContext): EitherTF[T] = EitherT.right(Future.successful(rightValue))

    def left[T](error: ApplicationError)(implicit ec: ExecutionContext): EitherTF[T] = EitherT.left(Future.successful(error))

  }

  private def eitherTFMonoid[T](implicit ec: ExecutionContext): Monoid[EitherTF[List[T]]] = new Monoid[EitherTF[List[T]]] {
    override def empty: EitherTF[List[T]] =
      EitherT.right(Future.successful(List.empty[T]))

    override def combine(x: EitherTF[List[T]], y: EitherTF[List[T]]): EitherTF[List[T]] =
      for {
        v1 <- x
        v2 <- y
      } yield v1 ++ v2
  }

  implicit class ListEitherTFExtension[T](val eitherTFList: Iterable[EitherTF[T]]) extends AnyVal {
    def foldable(implicit ec: ExecutionContext): EitherTF[List[T]] = {
      Foldable[List].foldMap(eitherTFList.toList)(a => a.map(List(_)))(eitherTFMonoid[T])
    }
  }

  implicit class EitherTFExtensions[T](val EitherT: EitherTF[T]) extends AnyVal {

    def toActionResult(implicit ec: ExecutionContext): Future[Result] = EitherT.value.map {
      case Left(error)        => BadRequest(error.toJson)
      case Right(ok: Result)  => ok
      case Right(msg: String) => Ok(msg)
      case Right(_: Unit)     => Ok(Json.obj("status" -> true))
      case Right(a) =>
        Logger.debug(s"$a was ignored in Result")
        Ok(Json.obj("status" -> true))
    }

    def toActionResult(f: ApplicationError => Result)(implicit ec: ExecutionContext): Future[Result] = EitherT.value.map {
      case Left(error)        => f(error)
      case Right(ok: Result)  => ok
      case Right(msg: String) => Ok(msg)
      case Right(_: Unit)     => Ok(Json.obj("status" -> true))
      case Right(a) =>
        Logger.debug(s"$a was ignored in Result")
        Ok(Json.obj("status" -> true))
    }
  }

  implicit class FutureTExtensions[T](val value: Future[T]) extends AnyVal {

    def toEitherTF(implicit ec: ExecutionContext): EitherTF[T] = ErrorHandler.futureToEitherTFWithErrorHandling(value)

  }

  implicit class FutureTXorExtensions[T](val value: Future[Either[ApplicationError, T]]) extends AnyVal {

    def toEitherTF: EitherTF[T] = EitherT(value)

  }

  implicit class EitherExtension[T](val either: Either[ApplicationError, T]) extends AnyVal {

    def toEitherTF(implicit ec: ExecutionContext): EitherTF[T] = EitherT.fromEither(either)

  }

}
