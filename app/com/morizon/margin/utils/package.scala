package com.morizon.margin

import cats.data.EitherT
import com.morizon.margin.errors.ApplicationError

import scala.concurrent.Future

package object utils {
  type EitherTF[R] = EitherT[Future, ApplicationError, R]
  type EmptyEitherTF = EitherTF[Unit]
}
