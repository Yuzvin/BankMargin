package com.morizon.margin.utils

import com.morizon.margin.models.{Margin, MarginRange, MarginRangeType}
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import net.ceedubs.ficus.readers.ValueReader

object ConfigValueReaders {

  implicit val marginReader: ValueReader[Margin] = ValueReader.relative { config =>
    Margin(
      marginValue = config.as[Double]("marginValue"),
      ranges = config.as[Seq[MarginRange]]("ranges")
    )
  }

  implicit val marginTypeReader = ValueReader.relative { config =>
    MarginRangeType(
      name = config.as[String]("name"),
      units = config.as[String]("units")
    )
  }

}
