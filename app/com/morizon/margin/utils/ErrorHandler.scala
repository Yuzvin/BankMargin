package com.morizon.margin.utils

import java.sql.SQLException

import cats.implicits._
import com.morizon.margin.errors.{InternalDatabaseError, InternalServerError}
import com.morizon.margin.utils.EitherTFUtils._
import play.api.Logger
import slick.SlickException

import scala.concurrent.{ExecutionContext, Future}

object ErrorHandler {
  def futureToEitherTFWithErrorHandling[T](block: => Future[T])(implicit ec: ExecutionContext): EitherTF[T] = {
      block.map{
        blockResult =>
          blockResult.asRight
      }.recover{
        case e @(_: SlickException | _: SQLException) =>
          Logger.error(s"Database exception = $e",e)
          InternalDatabaseError.asLeft
        case e: Throwable =>
          Logger.error(s"Internal server exception = $e",e)
          InternalServerError.asLeft
      }.toEitherTF
    }

}
