package com.morizon.margin

import com.google.inject.AbstractModule
import com.morizon.margin.dao._
import com.morizon.margin.services._

class CommonModule extends AbstractModule {

  override def configure() = {

    // Eager singletons
    bind(classOf[ApplicationStart]).asEagerSingleton()

    //Services
    bind(classOf[MockingService]).to(classOf[MockingServiceImplementation])
    bind(classOf[MarginService]).to(classOf[MarginServiceImplementation])
    bind(classOf[ValidationService]).to(classOf[ValidationServiceImplementation])
    bind(classOf[MarginRangeTypeService]).to(classOf[MarginRangeTypeServiceImplementation])

    //DAO
    bind(classOf[MarginsDao]).to(classOf[MarginsDaoImplementation])
    bind(classOf[MarginRangeDao]).to(classOf[MarginRangeDaoImplementation])
    bind(classOf[MarginRangeTypesDao]).to(classOf[MarginRangeTypesDaoImplementation])

  }

}
