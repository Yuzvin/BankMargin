package com.morizon.margin.models

case class MarginSearchCriterion(rangeTypeName: String, rangeValue: Long)
