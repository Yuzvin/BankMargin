package com.morizon.margin.models

object MarginRangeTypeNames extends {
  val PersonalShare = "Personal share"
  val LoanAmount = "Loan amount"
  val PaymentMonth = "Payment month"
}
