package com.morizon.margin.models

import io.swagger.annotations.ApiModel

@ApiModel
case class Margin(marginValue: Double, ranges: Seq[MarginRange], id: Option[Long] = None)

