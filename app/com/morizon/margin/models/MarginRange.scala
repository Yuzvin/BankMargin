package com.morizon.margin.models

import io.swagger.annotations.ApiModel

@ApiModel
case class MarginRange(rangeType: MarginRangeType, min: Long, max: Long, id: Option[Long] = None)

