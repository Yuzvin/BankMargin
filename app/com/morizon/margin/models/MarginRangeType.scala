package com.morizon.margin.models

import io.swagger.annotations.ApiModel

@ApiModel
case class MarginRangeType(name: String, units: String, id: Option[Long] = None)

