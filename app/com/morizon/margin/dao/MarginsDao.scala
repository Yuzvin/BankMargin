package com.morizon.margin.dao

import javax.inject.{Inject, Singleton}

import cats.implicits._
import com.morizon.margin.models.{Margin, MarginRange, MarginRangeType, MarginSearchCriterion}
import com.morizon.margin.slick.tables.queries.{MarginQueries, MarginToRangesQueries}
import com.morizon.margin.utils.EitherTF
import com.morizon.margin.utils.EitherTFUtils._
import play.api.db.slick.DatabaseConfigProvider

import scala.concurrent.ExecutionContext.Implicits.global

trait MarginsDao {

  def findMargins(searchCriteria: Seq[MarginSearchCriterion]): EitherTF[Seq[Margin]]

  def insertMargin(margin: Margin): EitherTF[Seq[Long]]

  def insertMarginValue(marginValue: Double): EitherTF[Long]

}

@Singleton
class MarginsDaoImplementation @Inject()(marginRangeTypesDao: MarginRangeTypesDao,
                                         marginRangeDao: MarginRangeDao,
                                         protected val dbConfigProvider: DatabaseConfigProvider)
    extends MarginsDao
    with MarginQueries
    with MarginToRangesQueries {

  override def findMargins(searchCriteria: Seq[MarginSearchCriterion]): EitherTF[Seq[Margin]] = {
    db.run(selectMargins(searchCriteria)).map(selectResultsToMargins(_, searchCriteria.size)).toEitherTF
  }

  override def insertMargin(margin: Margin): EitherTF[Seq[Long]] = {
    for {
      marginId <- insertMarginValue(margin.marginValue)
      rangesId <- margin.ranges.map(range => insertMarginRange(range, marginId)).foldable
    } yield rangesId
  }

  override def insertMarginValue(marginValue: Double): EitherTF[Long] = {
    db.run(insertMarginValue(marginValueToMarginValueRow(marginValue))).toEitherTF
  }

  private def insertMarginRange(marginRange: MarginRange, marginId: Long): EitherTF[Long] = {
    for {
      marginTypeId  <- marginRangeTypesDao.insertRangeType(marginRange.rangeType)
      marginRangeId <- marginRangeDao.insertMarginRange(marginRange, marginId, marginTypeId)
      _             <- db.run(insertMarginToRange(marginToRangeRow(marginId, marginRangeId))).toEitherTF
    } yield {
      marginRangeId
    }
  }

  private def selectResultsToMargins(dbResults: Seq[DBSearchResult], searchCriteriaSize: Int) = {
    dbResults
      .groupBy(_.marginId)
      .collect {
        case (_, results) if results.size == searchCriteriaSize =>
          results
            .map { result =>
              val marginRangeType = MarginRangeType(result.marginTypeName, result.marginTypeUnit)
              result.marginValue -> MarginRange(marginRangeType, result.marginMinValue, result.marginMaxValue)
            }
            .groupBy { case (marginValue, _) => marginValue }
            .map { case (value, valWithRanges) => Margin(value, valWithRanges.map { case (_, range) => range }) }
      }
      .flatten
      .toSeq
  }

  private def marginValueToMarginValueRow(marginValue: Double) = {
    MarginValueRow(
      id = 0,
      value = marginValue
    )
  }

  private def marginToRangeRow(marginId: Long, rangeId: Long): MarginToRangeRow = {
    MarginToRangeRow(
      marginId = marginId,
      marginRangeId = rangeId,
      id = 0
    )
  }



}
