package com.morizon.margin.dao

import javax.inject.{Inject, Singleton}

import com.morizon.margin.models.MarginRangeType
import com.morizon.margin.slick.tables.queries.MarginRangeTypesQueries
import com.morizon.margin.utils.EitherTF
import com.morizon.margin.utils.EitherTFUtils._
import play.api.db.slick.DatabaseConfigProvider

import scala.concurrent.ExecutionContext.Implicits.global

trait MarginRangeTypesDao {

  def insertRangeType(rangeType: MarginRangeType): EitherTF[Long]

  def getRangeTypes(): EitherTF[Seq[MarginRangeType]]

  def findRangeType(id: Long): EitherTF[Option[MarginRangeType]]

}

@Singleton
class MarginRangeTypesDaoImplementation @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends MarginRangeTypesDao with MarginRangeTypesQueries {

  override def insertRangeType(rangeType: MarginRangeType): EitherTF[Long] = {
    rangeType.id match {
      case Some(id) => EitherTF(id)
      case None => db.run(insertRangeType(toMarginRangeTypeRow(rangeType))).toEitherTF
    }
  }

  override def getRangeTypes(): EitherTF[Seq[MarginRangeType]] = {
    db.run(selectAllMarginRangeTypes()).map(_.map(rowToMarginRangeType)).toEitherTF
  }

  override def findRangeType(id: Long) = {
    db.run(selectMarginRangeType(id)).map(_.map(rowToMarginRangeType)).toEitherTF
  }

  def rowToMarginRangeType(marginRangeTypeRow: MarginRangeTypeRow) = {
    MarginRangeType(
      name = marginRangeTypeRow.name,
      units = marginRangeTypeRow.unit,
      id = Some(marginRangeTypeRow.id)
    )
  }

}
