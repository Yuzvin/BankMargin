package com.morizon.margin.dao

import cats.implicits._
import com.google.inject.{Inject, Singleton}
import com.morizon.margin.models.{MarginRange, MarginRangeType}
import com.morizon.margin.slick.tables.queries.{MarginRangeQueries, MarginRangeTypesQueries}
import com.morizon.margin.utils.EitherTF
import com.morizon.margin.utils.EitherTFUtils._
import play.api.db.slick.DatabaseConfigProvider

import scala.concurrent.ExecutionContext.Implicits.global

trait MarginRangeDao {

  def insertMarginRange(marginRange: MarginRange, marginId: Long, marginTypeId: Long): EitherTF[Long]

  def getAllMarginRanges(): EitherTF[Seq[MarginRange]]

}

@Singleton
class MarginRangeDaoImplementation @Inject()(marginRangeTypesDao: MarginRangeTypesDao,
                                             protected val dbConfigProvider: DatabaseConfigProvider)
    extends MarginRangeDao
    with MarginRangeQueries
    with MarginRangeTypesQueries {

  def insertMarginRange(marginRange: MarginRange, marginId: Long, marginTypeId: Long): EitherTF[Long] = {
    db.run(insertMarginRangeRow(rangeToMarginRangeRow(marginRange, marginTypeId))).toEitherTF
  }

  override def getAllMarginRanges(): EitherTF[Seq[MarginRange]] = {
    for {
      marginRangesRows          <- db.run(selectAllMarginRanges()).toEitherTF
      marginRangesRowsWithTypes <- attachMarginRangeTypeToRows(marginRangesRows)
    } yield {
      marginRangesRowsWithTypes
    }
  }

  private def attachMarginRangeTypeToRows(marginRangesRows: Seq[MarginRangeRow]): EitherTF[Seq[MarginRange]] = {
    marginRangesRows.map { rangeRow =>
      marginRangeTypesDao.findRangeType(rangeRow.marginRangeTypeId).map { rangeTypeOpt =>
        rangeTypeOpt.map(rangeType => rowToMarginRange(rangeRow, rangeType))
      }
    }.foldable.map(_.flatten)
  }

  private def rangeToMarginRangeRow(marginRange: MarginRange, marginTypeId: Long) = {
    MarginRangeRow(
      id = marginRange.id.getOrElse(0),
      marginRangeTypeId = marginTypeId,
      minValue = marginRange.min,
      maxValue = marginRange.max
    )
  }

  private def rowToMarginRange(marginRangeRow: MarginRangeRow, marginRangeType: MarginRangeType) = {
    MarginRange(rangeType = marginRangeType,
                min = marginRangeRow.minValue,
                max = marginRangeRow.maxValue,
                id = Some(marginRangeRow.id))
  }

}
