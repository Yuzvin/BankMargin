package com.morizon.margin.controllers

import cats.implicits._
import com.google.inject.{Inject, Singleton}
import com.morizon.margin.controllers.json.JsonFormats._
import com.morizon.margin.services.{MarginService, ValidationService}
import com.morizon.margin.utils.EitherTFUtils._
import io.swagger.annotations._
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

import scala.concurrent.ExecutionContext.Implicits.global

@Api("Margins")
@Singleton
class MarginsController @Inject()(marginService: MarginService,
                                  validationService: ValidationService,
                                  cc: ControllerComponents)
    extends AbstractController(cc) with ApiDescription {

  override def findMargins(@ApiParam(value = "Month of the payment", required = true) month: Int,
                           @ApiParam(value = "Personal share, %", required = true) share: Int,
                           @ApiParam(value = "Loan amount, PLN", required = true) loan: Long): Action[AnyContent] = Action.async {
    val result = for {
      _       <- validationService.validateSearchValues(month, share, loan).toEitherTF
      margins <- marginService.findMargins(month, share, loan)
    } yield {
      Ok(Json.toJson(margins))
    }
    result.toActionResult
  }

}
