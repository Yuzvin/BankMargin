package com.morizon.margin.controllers.json

import com.morizon.margin.models.{Margin, MarginRange, MarginRangeType}
import play.api.libs.json.Json

object JsonFormats {

  implicit val marginRangeTypeFormat = Json.format[MarginRangeType]
  implicit val marginRangeFormat     = Json.format[MarginRange]
  implicit val marginFormat          = Json.format[Margin]

}
