package com.morizon.margin.controllers

import com.google.inject.Inject
import play.api.mvc.{AbstractController, ControllerComponents}

class ApiSpecs @Inject() (cc: ControllerComponents) extends AbstractController(cc) {

  def docs = Action {
    Redirect("/docs/swagger-ui/index.html?url=/swagger.json")
  }
}

