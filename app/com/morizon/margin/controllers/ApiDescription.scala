package com.morizon.margin.controllers

import com.morizon.margin.errors.ApplicationError
import com.morizon.margin.models.Margin
import io.swagger.annotations.{ApiOperation, ApiResponse, ApiResponses}
import play.api.mvc.{Action, AnyContent}

trait ApiDescription {

  @ApiOperation(value = "Find margins by payment month, personal share and loan amount", httpMethod = "GET")
  @ApiResponses(Array(
    new ApiResponse(code = 200, response = classOf[Array[Margin]], message = "Margin values with ranges"),
    new ApiResponse(code = 400, response = classOf[ApplicationError], message = "Error with message")
  ))
  def findMargins(month: Int, share: Int, loan: Long): Action[AnyContent]
}
