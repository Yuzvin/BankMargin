import play.sbt.PlayImport._
import sbt._

object ProjectDependencies {

  private val slick = Seq(
    "com.typesafe.play" %% "play-slick"            % "3.0.0",
    "com.typesafe.play" %% "play-slick-evolutions" % "3.0.0",
    "com.h2database"    % "h2"                     % "1.4.193",
    "org.postgresql"    % "postgresql"             % "9.4.1212"
  )

  private val tests = Seq(
    specs2          % Test,
    "org.scalatest" %% "scalatest" % "3.0.4" % "test",
    "org.scalatestplus.play" % "scalatestplus-play_2.12" % "3.1.2" % "test"
  )

  private val playCommon = Seq(ws, guice)

  private val configuration = Seq(
    "com.iheart" %% "ficus" % "1.4.2"
  )

  private val functionalLibs = Seq(
    "org.typelevel" %% "cats-core" % "1.0.0-MF"
  )

  private val documentation = Seq(
    "io.swagger" % "swagger-play2_2.12" % "1.6.0",
    "org.webjars" % "swagger-ui" % "3.0.19"
  )

  val dependencies = playCommon ++ tests ++ slick ++ configuration ++ functionalLibs ++ documentation

}
