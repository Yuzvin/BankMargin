# BankMargin
## How to run
1. Go to project root directory, and then run from command line:
```
sbt run
```
It assumes you’ve installed [sbt](http://www.scala-sbt.org/release/docs/Setup.html) 0.13.13 or later.
2. Open http://localhost:9000/

## How to run tests
From root directory of project run from command line:
```
sbt test
```
