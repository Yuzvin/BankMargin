package com.morizon.margin.service

import com.morizon.margin.context.MarginApplication
import com.morizon.margin.dao.MarginsDao
import com.morizon.margin.extensions.AwaitExtensions._
import com.morizon.margin.models.{Margin, MarginRangeTypeNames, MarginSearchCriterion}
import com.morizon.margin.services.MarginService
import com.morizon.margin.utils.EitherTFUtils.EitherTF
import org.mockito.Matchers.{eq => _eq}
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import play.api.inject._
import play.api.test.PlaySpecification

import scala.concurrent.ExecutionContext.Implicits.global

class MarginServiceSpecification extends PlaySpecification with MockitoSugar with MarginApplication {

  lazy val marginsDaoMock = mock[MarginsDao]
  lazy val marginService = testApplication.injector.instanceOf[MarginService]

  override lazy val testApplication = applicationBuilder.overrides(bind[MarginsDao].to(marginsDaoMock)).build()

  "MarginService" should {
    "find margins" in {
      val searchCriteria = Seq(
        MarginSearchCriterion(MarginRangeTypeNames.PaymentMonth, 14),
        MarginSearchCriterion(MarginRangeTypeNames.PersonalShare, 20),
        MarginSearchCriterion(MarginRangeTypeNames.LoanAmount, 50000)
      )

      when(marginsDaoMock.findMargins(searchCriteria)).thenReturn(EitherTF(Seq.empty[Margin]))

      val findResult = marginService.findMargins(14, 20, 50000).await()
      findResult.isRight === true
      verify(marginsDaoMock, times(1)).findMargins(_eq(searchCriteria))
      ok

    }
  }

}
