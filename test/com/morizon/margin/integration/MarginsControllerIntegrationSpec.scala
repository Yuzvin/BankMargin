package com.morizon.margin.integration

import com.morizon.margin.context.{MarginApplication, WithOwnInMemoryDatabase}
import com.morizon.margin.controllers.MarginsController
import com.morizon.margin.controllers.json.JsonFormats._
import com.morizon.margin.errors.ErrorMessages
import com.morizon.margin.models.{Margin, MarginRange, MarginRangeTypeNames}
import com.morizon.margin.services.MockingService
import com.typesafe.config.Config
import play.api.test.{FakeRequest, PlaySpecification}



class MarginsControllerIntegrationSpec extends PlaySpecification with MarginApplication with WithOwnInMemoryDatabase {

  lazy val config = testApplication.injector.instanceOf[Config]
  lazy val controller = testApplication.injector.instanceOf[MarginsController]
  lazy val mockingService = testApplication.injector.instanceOf[MockingService]

  "Margins controller" should {
    "find expected margin for 24 various midpoint range cases" in {
      mockedMargins.map(mockedMargin => findMargin(mockedMargin, getMidpoint) === mockedMargin.marginValue)
    }

    "find expected margin for 24 various min range edge cases" in {
      mockedMargins.map(mockedMargin => findMargin(mockedMargin, getMin) === mockedMargin.marginValue)
    }

    "find expected margin for 24 various max range edge cases" in {
      mockedMargins.map(mockedMargin => findMargin(mockedMargin, getMax) === mockedMargin.marginValue)
    }

    "find expected margin with paymentMonth <= 12" in {
      findMargin(SearchData(10, 11, 1)) === 1.1
    }

    "return error if personal share is wrong" in {
      getErrorMessageAfterFailedMarginSearch(SearchData(10, 5, 10000)) === ErrorMessages.invalidPersonalShare(5)
      getErrorMessageAfterFailedMarginSearch(SearchData(10, 0, 10000)) === ErrorMessages.invalidPersonalShare(0)
      getErrorMessageAfterFailedMarginSearch(SearchData(10, -1, 10000)) === ErrorMessages.invalidPersonalShare(-1)
      getErrorMessageAfterFailedMarginSearch(SearchData(10, 101, 10000)) === ErrorMessages.invalidPersonalShare(101)
    }

    "return error if payment month is wrong" in {
      val maxPaymentMonth = config.getInt("max.payment-month") + 1
      getErrorMessageAfterFailedMarginSearch(SearchData(-1, 50, 10000)) === ErrorMessages.invalidPaymentMonth(-1)
      getErrorMessageAfterFailedMarginSearch(SearchData(maxPaymentMonth, 50, 10000)) === ErrorMessages.invalidPaymentMonth(maxPaymentMonth)
    }

    "return error if loan amount is wrong" in {
      val maxLoanAmount = config.getInt("max.loan") + 1
      getErrorMessageAfterFailedMarginSearch(SearchData(10, 50, 0)) === ErrorMessages.invalidLoanAmount(0)
      getErrorMessageAfterFailedMarginSearch(SearchData(10, 50, maxLoanAmount)) === ErrorMessages.invalidLoanAmount(maxLoanAmount)
    }

  }

  lazy val mockedMargins = mockingService.getMockedMargins()

  private case class SearchData(month: Int, share: Int, loan: Long)

  private def extractSearchDataFromMockedMargin(margin: Margin, calcRangeValue: (MarginRange) => Long) = {
    val searchDataOpt = for {
      month <- margin.ranges.find(_.rangeType.name == MarginRangeTypeNames.PaymentMonth).map(calcRangeValue)
      share <- margin.ranges.find(_.rangeType.name == MarginRangeTypeNames.PersonalShare).map(calcRangeValue)
      loan  <- margin.ranges.find(_.rangeType.name == MarginRangeTypeNames.LoanAmount).map(calcRangeValue)
    } yield {
      SearchData(month.toInt, share.toInt, loan)
    }

    searchDataOpt.isDefined === true
    searchDataOpt.get
  }

  private def findMargin(searchData: SearchData): Double = {
    val resultFuture = controller.findMargins(searchData.month, searchData.share, searchData.loan)(FakeRequest())
    val margins = contentAsJson(resultFuture).as[Seq[Margin]]
    margins.size === 1
    margins.head.marginValue
  }

  private def findMargin(mockedMargin: Margin, calcRangeValue: (MarginRange) => Long): Double = {
    val searchData = extractSearchDataFromMockedMargin(mockedMargin, calcRangeValue)
    findMargin(searchData)
  }

  private def getErrorMessageAfterFailedMarginSearch(searchData: SearchData): String = {
    val resultFuture = controller.findMargins(searchData.month, searchData.share, searchData.loan)(FakeRequest())
    (contentAsJson(resultFuture) \ "errorMessage").as[String]
  }

  private def getMidpoint(marginRange: MarginRange): Long = (marginRange.min + marginRange.max) / 2

  private def getMin(marginRange: MarginRange): Long = marginRange.min

  private def getMax(marginRange: MarginRange): Long = marginRange.max - 1

}
