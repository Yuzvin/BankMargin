package com.morizon.margin.dao

import com.morizon.margin.context.MarginApplication
import com.morizon.margin.dao.context.MarginsDaoCommonContext
import com.morizon.margin.extensions.AwaitExtensions._
import play.api.test.PlaySpecification

class MarginRangeTypesDaoSpec extends PlaySpecification with MarginApplication with MarginsDaoCommonContext {

  "MarginRangeTypesDao" should {
    "add and get margin range types" in {
      val addMarginRangeTypeResult = marginRangeTypeDao.insertRangeType(shareMarginRangeType).await()
      addMarginRangeTypeResult.isRight === true

      val newMarginRangeTypeId = addMarginRangeTypeResult.right.get

      val findCreatedMarginRangeType = marginRangeTypeDao.findRangeType(newMarginRangeTypeId).await()
      findCreatedMarginRangeType.isRight === true
      val foundMarginRangeTypeOpt = findCreatedMarginRangeType.right.get
      foundMarginRangeTypeOpt should beSome

      val foundMarginRangeType = foundMarginRangeTypeOpt.get

      foundMarginRangeType.id === Some(newMarginRangeTypeId)
      foundMarginRangeType.name === shareMarginRangeType.name
      foundMarginRangeType.units === shareMarginRangeType.units
    }
  }

}
