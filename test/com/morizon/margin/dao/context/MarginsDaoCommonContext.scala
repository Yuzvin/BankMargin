package com.morizon.margin.dao.context

import com.morizon.margin.context.MarginApplication
import com.morizon.margin.dao.{MarginRangeDao, MarginRangeTypesDao, MarginsDao}
import com.morizon.margin.models.{MarginRange, MarginRangeType, MarginRangeTypeNames}

trait MarginsDaoCommonContext extends MarginApplication {

  lazy val marginsDao         = testApplication.injector.instanceOf[MarginsDao]
  lazy val marginRangeDao     = testApplication.injector.instanceOf[MarginRangeDao]
  lazy val marginRangeTypeDao = testApplication.injector.instanceOf[MarginRangeTypesDao]

  val percentUnits = "%"
  val plnUnits     = "PLN"
  val monthUnits   = "month"

  val shareMarginRangeType = MarginRangeType(MarginRangeTypeNames.PersonalShare, percentUnits)
  val loanMarginRangeType  = MarginRangeType(MarginRangeTypeNames.LoanAmount, plnUnits)
  val monthMarginRangeType = MarginRangeType(MarginRangeTypeNames.PaymentMonth, monthUnits)

  val minShare = 10
  val maxShare = 20
  val testShareMarginRange = MarginRange(shareMarginRangeType, minShare, maxShare)

  val minLoan = 10000
  val maxLoan = 15000
  val testLoanMarginRange = MarginRange(loanMarginRangeType, minLoan, maxLoan)

  val minMonth = 13
  val maxMonth = 100
  val testMonthMarginRange = MarginRange(monthMarginRangeType, minMonth, maxMonth)

  val testMarginRanges = Seq(testShareMarginRange, testLoanMarginRange, testMonthMarginRange)
}
