package com.morizon.margin.dao

import com.morizon.margin.context.{MarginApplication, WithoutMockedMargins}
import com.morizon.margin.dao.context.MarginsDaoCommonContext
import com.morizon.margin.extensions.AwaitExtensions._
import play.api.test.PlaySpecification

class MarginRangeDaoSpec extends PlaySpecification with MarginApplication with WithoutMockedMargins with MarginsDaoCommonContext {

  "MarginRangeDaoSpec" should {
    "add and get margin ranges" in {

      val testMarginValue = 7.77
      val insertMarginValueResult = marginsDao.insertMarginValue(testMarginValue).await()
      insertMarginValueResult.isRight === true
      val newMarginId = insertMarginValueResult.right.get

      val insertMarginRangeTypeResult = marginRangeTypeDao.insertRangeType(shareMarginRangeType).await()
      insertMarginRangeTypeResult.isRight === true
      val newMarginTypeId = insertMarginRangeTypeResult.right.get

      val insertMarginRangeResult = marginRangeDao.insertMarginRange(testShareMarginRange, newMarginId, newMarginTypeId).await()
      insertMarginRangeResult.isRight === true
      val newMarginRangeId = insertMarginRangeResult.right.get

      val allMarginRangesResult = marginRangeDao.getAllMarginRanges().await()
      allMarginRangesResult.isRight === true
      val allMarginRanges = allMarginRangesResult.right.get

      val foundMarginRangeOpt = allMarginRanges.find(_.id.contains(newMarginRangeId))
      foundMarginRangeOpt must beSome
      val foundMarginRange = foundMarginRangeOpt.get

      foundMarginRange.id === Some(newMarginRangeId)
      foundMarginRange.rangeType.copy(id = None) === shareMarginRangeType
      foundMarginRange.max === testShareMarginRange.max
      foundMarginRange.min === testShareMarginRange.min

    }
  }

}
