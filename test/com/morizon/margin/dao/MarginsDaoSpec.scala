package com.morizon.margin.dao

import com.morizon.margin.context.{MarginApplication, WithoutMockedMargins}
import com.morizon.margin.dao.context.MarginsDaoCommonContext
import com.morizon.margin.extensions.AwaitExtensions._
import com.morizon.margin.models._
import play.api.test.PlaySpecification

class MarginsDaoSpec extends PlaySpecification with MarginApplication with WithoutMockedMargins with MarginsDaoCommonContext {

  "MarginsDAO" should {
    "add and search margin" in {

      val testMarginValue = 1.23
      val testMargin = Margin(testMarginValue, testMarginRanges)
      val addMarginResult = marginsDao.insertMargin(testMargin).await()

      addMarginResult should beRight

      val searchCriteria = Seq(
        MarginSearchCriterion(MarginRangeTypeNames.PersonalShare, 15),
        MarginSearchCriterion(MarginRangeTypeNames.LoanAmount, 11000),
        MarginSearchCriterion(MarginRangeTypeNames.PaymentMonth, 24)
      )
      val searchMarginResult = marginsDao.findMargins(searchCriteria).await()
      searchMarginResult should beRight

      val foundMargins = searchMarginResult.right.get
      foundMargins.size !== 0
      foundMargins.exists(_.marginValue == testMarginValue) === true

      val foundRanges = foundMargins.find(_.marginValue == testMarginValue).get.ranges
      foundRanges.size === 3

      foundRanges.exists(r => r.rangeType.name == MarginRangeTypeNames.PersonalShare &&
      r.rangeType.units == percentUnits && r.min == minShare && r.max == maxShare) === true

      foundRanges.exists(r => r.rangeType.name == MarginRangeTypeNames.LoanAmount &&
      r.rangeType.units == plnUnits && r.min == minLoan && r.max == maxLoan) === true

      foundRanges.exists(r => r.rangeType.name == MarginRangeTypeNames.PaymentMonth &&
      r.rangeType.units == monthUnits && r.min == minMonth && r.max == maxMonth) === true

    }
  }

}
