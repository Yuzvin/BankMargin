package com.morizon.margin.extensions

import com.morizon.margin.errors.ApplicationError
import com.morizon.margin.utils.EitherTF

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object AwaitExtensions {

  final val DURATION = 10.seconds

  implicit class AwaitFutureExtension[T](val future: Future[T]) extends AnyVal {

    def await(): T = {
      Await.result(future, DURATION)
    }

    def await(duration: Duration): T = {
      Await.result(future, duration)
    }

  }

  implicit class AwaitEitherTFExtension[T](val eitherTF: EitherTF[T]) extends AnyVal {

    def await(): Either[ApplicationError, T] = {
      Await.result(eitherTF.value, DURATION)
    }

  }


}