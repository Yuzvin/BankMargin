package com.morizon.margin.context

import org.scalatest.mockito.MockitoSugar
import org.specs2.specification.Scope
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.inject.{Binding, Module}


trait MarginApplication extends Scope with WithOwnInMemoryDatabase with MockitoSugar {

  lazy val fakeModule = new FakeModule()

  lazy val applicationBuilder = new GuiceApplicationBuilder().configure(overrideConf)

  lazy val testApplication = applicationBuilder.build()

  class FakeModule extends Module {

    var additionalBindings: Seq[Binding[_]] = Nil

    override def bindings(environment: play.api.Environment, configuration: Configuration) = additionalBindings
  }

}

trait WithOwnInMemoryDatabase { self: MarginApplication =>

  lazy val withOwnMemoryDatabaseConf: Map[String, Any] = Map[String, Any](
    "slick.dbs.default.db.url" -> s"jdbc:h2:mem:${System.nanoTime};MODE=PostgreSQL;MVCC=true;LOCK_TIMEOUT=10000"
  )

  lazy val overrideConf: Map[String, Any] = withOwnMemoryDatabaseConf

}

trait WithoutMockedMargins { self: MarginApplication =>

  lazy val withoutMockedMarginsConfig = Map[String, Any](
    "margins" -> Nil
  )

  override lazy val overrideConf: Map[String, Any] = withoutMockedMarginsConfig

}
